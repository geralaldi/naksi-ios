//
//  RidingChildViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "RidingChildViewController.h"

@interface RidingChildViewController ()

@end

@implementation RidingChildViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _arriveAlert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Have you arrived yet?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"YES", nil];
    
    NSDate *futureDate = [NSDate dateWithTimeIntervalSinceNow:_expectedTime*60];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    _estimatedTimeLabel.text = [dateFormatter stringFromDate:futureDate];
    
    UITapGestureRecognizer *finishTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onFinishClicked:)];
    UITapGestureRecognizer *sosTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSOSClicked:)];
    
    [finishTapped setNumberOfTapsRequired:1];
    [sosTapped setNumberOfTapsRequired:1];
    [_finishButton addGestureRecognizer:finishTapped];
    [_sosButton addGestureRecognizer:sosTapped];
    
    NSInteger timerInterval = (_expectedTime == 1 || _expectedTime == 0) ? 1 : 60;
    
    _arrivalTimeLabel.text = [NSString stringWithFormat:@"in %li minutes", (long)_expectedTime];
    
    /**
     *  Set timer to be fired in timerInterval seconds, and add it to background thread
     *  so that when user move to another app, the timer is still valid
     */
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timerInterval target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

/**
 *  Method executed when timer is fired.
 *  Check if expected time left is 1 minute, then set arrival time label to be in seconds,
 *  otherwise, set it to be in minutesm and when it finishes, show finish popup dialog
 *
 *  @param timer Timer object
 */
- (void)timerFireMethod:(NSTimer *)timer
{
    if (timer.timeInterval == 60) {
        _arrivalTimeLabel.text = [NSString stringWithFormat:@"in %li minutes", (long)_expectedTime-1];
        _expectedTime = _expectedTime - 1;
        if (_expectedTime == 1) {
            [timer invalidate];
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
        }
    } else {
        if (_expectedTime != 0) {
            _expectedTimeInSeconds = _expectedTime * 60;
        }
        _arrivalTimeLabel.text = [NSString stringWithFormat:@"in %li seconds", (long)_expectedTimeInSeconds-1];
        _expectedTime = 0;
        if (_expectedTimeInSeconds != 0)
            _expectedTimeInSeconds = _expectedTimeInSeconds - 1;
        if (_expectedTimeInSeconds == 0) {
            [timer invalidate];
            [_delegate showFinishDialogOnContainer];
        }
    }
}

- (void)updateTravelDuration
{
    
}

/**
 *  Finish button was clicked, show Finish screen
 *
 *  @param sender Sender
 */
- (void)onFinishClicked:(id)sender
{
    FinishedViewController *finishedViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FinishedViewController"];
    [self presentViewController:finishedViewController animated:YES completion:nil];
}

/**
 *  SOS button was clicked, send delegate to execute show sos dialog
 *
 *  @param sender Sender
 */
- (void)onSOSClicked:(id)sender
{
    [_delegate showSOSDialogOnContainer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
