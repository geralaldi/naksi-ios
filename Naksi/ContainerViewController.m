//
//  ContainerViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "ContainerViewController.h"
#define Summary @"Summary"
#define Saved @"Saved"
#define Riding @"Riding"

@interface ContainerViewController ()

@end

@implementation ContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /**
     *  First view must be Summary View
     */
    if (_currentSegueIdentifier == nil) {
        _currentSegueIdentifier = Summary;
    }
    
    /**
     *  Perform Segue
     */
    [self performSegueWithIdentifier:_currentSegueIdentifier sender:nil];
}

/**
 *  Trigger show finish dialog on Settings View Controller
 */
- (void)showFinishDialogOnContainer
{
    [_delegate showFinishDialogOnSettings];
}

/**
 *  Trigger show sos dialog on Settings View Controller
 */
- (void)showSOSDialogOnContainer
{
    [_delegate showSOSDialogOnSettings];
}

/**
 *  Respond to click on save button in Summary View
 */
- (void)saveClicked
{
    [self performSegueWithIdentifier:Saved sender:_summaryViewController];
    [_delegate saveClicked];
}

/**
 *  Respond to click on change settings button in Saved View
 */
- (void)changeSettings
{
    [self performSegueWithIdentifier:Summary sender:_savedViewController];
    [_delegate enableEdit];
}

/**
 *  Preparing for segue, take action based on segue identifier
 *
 *  @param segue  segue object
 *  @param sender sender object
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /**
     *  If segue identifier is Summary, then assign destination view controller 
     *  as instance of Summary View Controller
     */
    if (([segue.identifier isEqualToString:Summary]) && !_summaryViewController) {
        _summaryViewController = segue.destinationViewController;
        _summaryViewController.delegate = self;
    }
    
    /**
     *  If segue identifier is Saved, then assign destination view controller
     *  as instance of Saved View Controller
     */
    if (([segue.identifier isEqualToString:Saved]) && !_savedViewController) {
        _savedViewController = segue.destinationViewController;
        _savedViewController.delegate = self;
    }
    
    /**
     *  If segue identifier is Riding, then assign destination view controller
     *  as instance of Riding View Controller;
     */
    if (([segue.identifier isEqualToString:Riding]) && !_ridingViewController) {
        _ridingViewController = segue.destinationViewController;
        _ridingViewController.expectedTime = _expectedTime;
        _ridingViewController.delegate = self;
    }
    
    /**
     *  Check if there is already a child view in the view stack,
     *  if there is already exist a child view, then swap the view,
     *  otherwise create a new view
     */
    if ([segue.identifier isEqualToString:Summary]) {
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.summaryViewController];
        } else {
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    } else if ([segue.identifier isEqualToString:Saved]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:_savedViewController];
    } else if ([segue.identifier isEqualToString:Riding]) {
        [_delegate isRiding];
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:_ridingViewController];
        } else {
            [self addChildViewController:_ridingViewController];
            UIView* destView = ((UIViewController *) _ridingViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [_ridingViewController didMoveToParentViewController:self];
        }
    }
}

/**
 *  Method to swap view controllers
 *
 *  @param fromViewController Source view controller
 *  @param toViewController   Destination view controller
 */
- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
    }];
}

- (void)swapViewControllers
{
    self.currentSegueIdentifier = ([self.currentSegueIdentifier isEqualToString:Summary]) ? Saved : Summary;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
