//
//  FinishedViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/7/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "FinishedViewController.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface FinishedViewController ()

@end

@implementation FinishedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /**
     *  Set Review Text View Border Color and width
     */
    [[_reviewTextView layer] setBorderColor:[UIColorFromRGB(0xE6E6E6) CGColor]];
    [[_reviewTextView layer] setBorderWidth:2.5];
    
    _locationManager = [[CLLocationManager alloc] init];
    
    _reviewTextView.delegate = self;
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    /**
     *  Keyboard notification
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [_locationManager startUpdatingLocation];
    
    /**
     *  Tap Gesture Recognizer for ImageViews
     */
    UITapGestureRecognizer *doneTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoneClicked:)];
    UITapGestureRecognizer *skipTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSkipClicked:)];
    UITapGestureRecognizer *oneTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneStarSelected:)];
    UITapGestureRecognizer *twoTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twoStarSelected:)];
    UITapGestureRecognizer *threeTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(threeStarSelected:)];
    UITapGestureRecognizer *fourTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fourStarSelected:)];
    UITapGestureRecognizer *fiveTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fiveStarSelected:)];
    
    [doneTapped setNumberOfTapsRequired:1];
    [skipTapped setNumberOfTapsRequired:1];
    [oneTapped setNumberOfTapsRequired:1];
    [twoTapped setNumberOfTapsRequired:1];
    [threeTapped setNumberOfTapsRequired:1];
    [fourTapped setNumberOfTapsRequired:1];
    [fiveTapped setNumberOfTapsRequired:1];
    
    [_doneButton addGestureRecognizer:doneTapped];
    [_skipButton addGestureRecognizer:skipTapped];
    [_starOne addGestureRecognizer:oneTapped];
    [_starTwo addGestureRecognizer:twoTapped];
    [_starThree addGestureRecognizer:threeTapped];
    [_starFour addGestureRecognizer:fourTapped];
    [_starFive addGestureRecognizer:fiveTapped];
    
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
    _twitterUsername.text = [NSString stringWithFormat:@"Finished, @%@", [[FHSTwitterEngine sharedEngine] authenticatedUsername]];
    
    /**
     *  Get Profile Picture in the background and set it to twitterAvatar
     */
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self getProfilePicture];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                @autoreleasepool {
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [_twitterAvatar setImage:_profilePicture];
                }
            });
        }
    });
    
    [self createInputAccessoryView];
    [_reviewTextView setInputAccessoryView:_inputAccessoryView];
    
    /**
     *  Disable Review Text View before rating
     */
    [_reviewTextView setUserInteractionEnabled:NO];
    [_reviewTextView setText:@"Rate first"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Finished Screen";
}

/**
 *  When keyboard appeared, scroll the views up
 */
- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    CGPoint activeTextFieldOrigin = _reviewTextView.frame.origin;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height = self.view.frame.size.height - keyboardSize.height - 40;
    
    if (!CGRectContainsPoint(visibleRect, activeTextFieldOrigin)) {
        CGPoint scrollPoint = CGPointMake(0.0, activeTextFieldOrigin.y - visibleRect.size.height + 120);
        [_scrollView setContentOffset:scrollPoint animated:NO];
    }
}

/**
 *  Reset Views position when keyboard is hidden
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    [_scrollView setContentOffset:CGPointZero];
}

/**
 *  Update location
 *
 *  @param manager   Location Manager
 *  @param locations Locations Array
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = (CLLocation *) [locations lastObject];
    NSTimeInterval locationAge = -[currentLocation.timestamp timeIntervalSinceNow];
    
    /**
     *  If location obtained in more than 5 seconds ago or accuracy is less than 0, then exit
     */
    if (locationAge > 5.0) return;
    if (currentLocation.horizontalAccuracy < 0) return;
    
    _locationTimerCount = _locationTimerCount + 5;
    
    NSLog(@"Arrival longitude : %f, latitude : %f", currentLocation.coordinate.longitude, currentLocation.coordinate.latitude);
    
    /**
     *  If latest location is empty or its accuracy is more than current location's,
     *  then assign current location as the latest location
     */
    if (_bestLocation == nil || _bestLocation.horizontalAccuracy > currentLocation.horizontalAccuracy) {
        _bestLocation = currentLocation;
        double longitude = _bestLocation.coordinate.longitude;
        double latitude = _bestLocation.coordinate.latitude;
        
        /**
         *  if current location accuracy is less than 100 meters or timeout is already reached
         *  then send location to server and stop updating location
         *  otherwise start timer that will fire 5 seconds since now
         */
        if (currentLocation.horizontalAccuracy <= 100 || _locationTimerCount == 5) {
            NSLog(@"Arrival message sent");
            [self sendToServer:longitude and:latitude];
            [_locationManager stopUpdatingLocation];
        } else {
            if (!_locationSent)
                [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(sendLocation:) userInfo:nil repeats:NO];
            _locationSent = YES;
        }
    }
}

/**
 *  Error when getting location, show an alert view to notify user
 *
 *  @param manager Location Manager
 *  @param error   Error message
 */
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"Location Error" message:@"Error when retrieving your location" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
    NSLog(@"%@", error);
}

/**
 *  Selector method for the timer
 *
 *  @param sender Timer
 */
- (void)sendLocation:(id)sender
{
    _locationSent = YES;
    [self sendToServer:_bestLocation.coordinate.longitude and:_bestLocation.coordinate.latitude];
    [_locationManager stopUpdatingLocation];
}

/**
 *  Send location coordinates to server
 *
 *  @param longitude Coordinate Longitude
 *  @param latitude  Coordinate Latitude
 */
- (void)sendToServer:(double)longitude and:(double)latitude
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDictionary *arriveDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:longitude], @"location_long", [NSNumber numberWithDouble:latitude], @"location_lat", [dateFormatter stringFromDate:date], @"arrived_time", nil];
    NSString *urlString = [NSString stringWithFormat:@"http://devel.wejoin.us/naksi/index.php/api/travel/arrive?api_key=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userToken"]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:urlString parameters:arriveDictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *) responseObject;
              BOOL status = (BOOL) [response objectForKey:@"status"];
              if (status) {
                  
              } else {
                  
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
          }];
}

/**
 *  To limit characters in Review TextView to 140 Chars
 *
 *  @param textView TextView used
 *  @param range    Range of Text
 *  @param text     Text
 *
 *  @return Whether limit is reached
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= 140;
}

/**
 *  Create done button in keyboard
 */
- (void)createInputAccessoryView
{
    if (!_inputAccessoryView) {
        _inputAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        _inputAccessoryView.backgroundColor = [UIColor whiteColor];
        _inputAccessoryView.alpha = 1;
        
        UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
        border.backgroundColor = [UIColor blackColor];
        
        UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        doneButton.frame = CGRectMake(255, 5, 60, 30.0);
        [doneButton setTitle: @"Done" forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [doneButton addTarget:self action:@selector(dismissKeyboard:)
             forControlEvents:UIControlEventTouchUpInside];
        [_inputAccessoryView addSubview:border];
        [_inputAccessoryView addSubview:doneButton];
    }
}

- (void)dismissKeyboard:(id)sender
{
    [_reviewTextView resignFirstResponder];
}

/**
 *  Done button is clicked, tweet the review, then show an alert that the review has been tweeted
 *
 *  @param sender Selector
 */
- (void)onDoneClicked:(id)sender
{
    [_reviewTextView setUserInteractionEnabled:NO];
    if ([[FHSTwitterEngine sharedEngine] isAuthorized] && _reviewTextView.text.length > 0) {
        [[FHSTwitterEngine sharedEngine] postTweet:_reviewTextView.text];
        [[[UIAlertView alloc] initWithTitle:@"Sent" message:@"Your review tweet is sent, you can quit the application now" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

/**
 *  Skip button is clicked, show Main Screen
 *
 *  @param sender Selector
 */
- (void)onSkipClicked:(id)sender
{
    [self performSegueWithIdentifier:@"Main" sender:self];
}

/**
 *  Dismiss AlertView then show Main Screen
 *
 *  @param alertView   AlertView shown
 *  @param buttonIndex Button index
 */
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self performSegueWithIdentifier:@"Main" sender:self];
}

/**
 *  One star is selected, change the images, then enable Review
 *
 *  @param sender Selector
 */
- (void)oneStarSelected:(id)sender
{
    _starOne.image = [UIImage imageNamed:@"rating_blue.png"];
    _starTwo.image = [UIImage imageNamed:@"rating_grey.png"];
    _starThree.image = [UIImage imageNamed:@"rating_grey.png"];
    _starFour.image = [UIImage imageNamed:@"rating_grey.png"];
    _starFive.image = [UIImage imageNamed:@"rating_grey.png"];
    [self enableReview:1];
}

/**
 *  Two star is selected, change the images, then enable Review
 *
 *  @param sender Selector
 */
- (void)twoStarSelected:(id)sender
{
    _starOne.image = [UIImage imageNamed:@"rating_blue.png"];
    _starTwo.image = [UIImage imageNamed:@"rating_blue.png"];
    _starThree.image = [UIImage imageNamed:@"rating_grey.png"];
    _starFour.image = [UIImage imageNamed:@"rating_grey.png"];
    _starFive.image = [UIImage imageNamed:@"rating_grey.png"];
    [self enableReview:2];
}

/**
 *  Three star is selected, change the images, then enable Review
 *
 *  @param sender Selector
 */
- (void)threeStarSelected:(id)sender
{
    _starOne.image = [UIImage imageNamed:@"rating_blue.png"];
    _starTwo.image = [UIImage imageNamed:@"rating_blue.png"];
    _starThree.image = [UIImage imageNamed:@"rating_blue.png"];
    _starFour.image = [UIImage imageNamed:@"rating_grey.png"];
    _starFive.image = [UIImage imageNamed:@"rating_grey.png"];
    [self enableReview:3];
}

/**
 *  Four star is selected, change the images, then enable Review
 *
 *  @param sender Selector
 */
- (void)fourStarSelected:(id)sender
{
    _starOne.image = [UIImage imageNamed:@"rating_blue.png"];
    _starTwo.image = [UIImage imageNamed:@"rating_blue.png"];
    _starThree.image = [UIImage imageNamed:@"rating_blue"];
    _starFour.image = [UIImage imageNamed:@"rating_blue.png"];
    _starFive.image = [UIImage imageNamed:@"rating_grey.png"];
    [self enableReview:4];
}

/**
 *  Five star is selected, change the images, then enable Review
 *
 *  @param sender Selector
 */
- (void)fiveStarSelected:(id)sender
{
    _starOne.image = [UIImage imageNamed:@"rating_blue.png"];
    _starTwo.image = [UIImage imageNamed:@"rating_blue.png"];
    _starThree.image = [UIImage imageNamed:@"rating_blue.png"];
    _starFour.image = [UIImage imageNamed:@"rating_blue.png"];
    _starFive.image = [UIImage imageNamed:@"rating_blue.png"];
    [self enableReview:5];
}

/**
 *  Enable Review Text View, set the template as placeholder
 *
 *  @param rate Rate Selected
 */
- (void)enableReview:(NSInteger)rate
{
    NSString *taxiCompany = [[NSUserDefaults standardUserDefaults] objectForKey:@"taxiCompany"];
    NSString *taxiNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"taxiNumber"];
    NSString *template = [NSString stringWithFormat:@"@NaksiApp Taxi %@ No. %@ (rating %i) ", taxiCompany, taxiNumber, rate];
    [_reviewTextView setUserInteractionEnabled:YES];
    [_reviewTextView setText:template];
}

/**
 *  Get Profile Picture from Twitter and save it to profilePicture
 */
- (void)getProfilePicture
{
    if ([[FHSTwitterEngine sharedEngine] isAuthorized]) {
        NSString *username = FHSTwitterEngine.sharedEngine.authenticatedUsername;
        _profilePicture = [[FHSTwitterEngine sharedEngine] getProfileImageForUsername:username andSize:FHSTwitterEngineImageSizeOriginal];
        if ([_profilePicture isKindOfClass:[NSError class]]) {
            [self getProfilePicture];
        }
    } else {
        
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
