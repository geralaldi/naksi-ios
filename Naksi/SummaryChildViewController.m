//
//  SummaryChildViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "SummaryChildViewController.h"

@interface SummaryChildViewController ()

@end

@implementation SummaryChildViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSaveClicked:)];
    [singleTap setNumberOfTapsRequired:1];
    [_saveButton addGestureRecognizer:singleTap];
    [self.view addSubview:_saveButton];
}

/**
 *  Save button clicked, send delegate to save the data
 *
 *  @param sender Sender
 */
- (void)onSaveClicked:(id)sender
{
    [_delegate saveClicked];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
