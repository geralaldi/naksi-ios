//
//  ContactAngel.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 2/26/14.
//  Copyright (c) 2014 Geraldi Kusuma Arnanto. All rights reserved.
//

#import "ContactAngel.h"

@implementation ContactAngel

- (id)initWithValue:(NSString *)contactName :(NSString *)contactNumber :(BOOL)clicked
{
    _contactName = contactName;
    _contactNumber = contactNumber;
    _clicked = clicked;
    
    return self;
}

/**
 *  Method for decoding object from file
 *
 *  @param aDecoder Object Decoder
 *
 *  @return id
 */
- (id)initWithCoder:(NSCoder *)aDecoder
{
    _contactName = [aDecoder decodeObjectForKey:@"contactName"];
    _contactNumber = [aDecoder decodeObjectForKey:@"contactNumber"];
    _clicked = [aDecoder decodeBoolForKey:@"clicked"];
    _listPosition = [aDecoder decodeIntegerForKey:@"listPosition"];
    
    return self;
}

/**
 *  Method for encoding object into file
 *
 *  @param coder Object encoder
 */
- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_contactNumber forKey:@"contactNumber"];
    [coder encodeObject:_contactName forKey:@"contactName"];
    [coder encodeBool:_clicked forKey:@"clicked"];
    [coder encodeInteger:_listPosition forKey:@"listPosition"];
}

/**
 *  Getter for clicked status of ContactAngel instance
 *
 *  @return clicked status
 */
- (BOOL)isClicked
{
    return _clicked;
}

/**
 *  Setter for clicked status of ContactAngel instance
 *
 *  @param clicked status
 */
-(void)setClicked:(BOOL)clicked
{
    _clicked = clicked;
}

@end
