//
//  WelcomeViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
    
    _twitterUsernameWelcome.text = [NSString stringWithFormat:@"Welcome, @%@", [[FHSTwitterEngine sharedEngine] authenticatedUsername]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self getProfilePicture];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                @autoreleasepool {
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [_twitterAvatar setImage:_profilePicture];
                }
            });
        }
    });
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextButtonClicked:)];
    [singleTap setNumberOfTapsRequired:1];
    [_nextButton addGestureRecognizer:singleTap];
    [self.view addSubview:_nextButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Welcome Screen";
}

/**
 *  Next button is clicked, show pick contact screen
 *
 *  @param sender Sender
 */
- (void)nextButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"PickContact" sender:self];
}

/**
 *  Get profile picture from twitter, if error occures, retry
 */
- (void)getProfilePicture
{
    if ([[FHSTwitterEngine sharedEngine] isAuthorized]) {
        NSString *username = FHSTwitterEngine.sharedEngine.authenticatedUsername;
        _profilePicture = [[FHSTwitterEngine sharedEngine] getProfileImageForUsername:username andSize:FHSTwitterEngineImageSizeOriginal];
        if ([_profilePicture isKindOfClass:[NSError class]]) {
            [self getProfilePicture];
        }
    } else {
        NSLog(@"Not authorized");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
