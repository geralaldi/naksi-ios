//
//  SOSPopupViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/15/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "SOSPopupViewController.h"

@interface SOSPopupViewController ()

@end

@implementation SOSPopupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _timerCount = 3;
    _timeLabel.text = [NSString stringWithFormat:@"%i", _timerCount];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    
    UITapGestureRecognizer *cancelTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onCancelButtonClicked:)];
    [cancelTapped setNumberOfTapsRequired:1];
    [_cancelButton addGestureRecognizer:cancelTapped];
}

/**
 *  Location updated, store the best location, then send it to server
 *
 *  @param manager   Location manager object
 *  @param locations Array of locations
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = (CLLocation *) [locations lastObject];
    NSTimeInterval locationAge = -[currentLocation.timestamp timeIntervalSinceNow];
    
    if (locationAge > 5.0) return;
    if (currentLocation.horizontalAccuracy < 0) return;
    
    NSLog(@"SOS longitude : %f, latitude : %f, accuracy : %f", currentLocation.coordinate.longitude, currentLocation.coordinate.latitude, currentLocation.horizontalAccuracy);
    
    /**
     *  If latest location is empty or its accuracy is more than current location
     *  assign current location as the latest location
     */
    if (_bestLocation == nil || _bestLocation.horizontalAccuracy > currentLocation.horizontalAccuracy) {
        _bestLocation = currentLocation;
        double longitude = _bestLocation.coordinate.longitude;
        double latitude = _bestLocation.coordinate.latitude;
        
        /**
         *  If current location accuracy is less than 100 meters, send the location to server
         *  and stop updating location, otherwise check for new location for 5 seconds
         */
        if (currentLocation.horizontalAccuracy <= 100) {
            NSLog(@"SOS message sent");
            [self sendToServer:longitude and:latitude];
            [_locationManager stopUpdatingLocation];
        } else {
            if (!_locationSent) {
                [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
                NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(sendLocation:) userInfo:nil repeats:NO];
                [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
            }
            _locationSent = YES;
        }
    }
}

/**
 *  Error when getting location, show an alert view
 *
 *  @param manager Location manager object
 *  @param error   Error message
 */
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"Location Error" message:@"Error when retrieving your location" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
    NSLog(@"%@", error);
}

/**
 *  Updating location timeout, send the coordinate
 *
 *  @param sender Sender
 */
- (void)sendLocation:(id)sender
{
    _locationSent = YES;
    [self sendToServer:_bestLocation.coordinate.longitude and:_bestLocation.coordinate.latitude];
    [_locationManager stopUpdatingLocation];
}

/**
 *  Send the latest location to server, then dismiss popup
 *
 *  @param longitude Location longitude
 *  @param latitude  Location latitude
 */
- (void)sendToServer:(double)longitude and:(double)latitude
{
    NSDictionary *travelDictionary = [[NSDictionary alloc] init];
    travelDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:latitude],
                        @"location_lat", [NSNumber numberWithDouble:longitude],
                        @"location_long", nil];
    
    NSString *urlString = [NSString stringWithFormat:@"http://devel.wejoin.us/naksi/index.php/api/travel/sos?api_key=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userToken"]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:urlString parameters:travelDictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *) responseObject;
              BOOL status = (BOOL) [response objectForKey:@"status"];
              if (status) {
                  [self.view removeFromSuperview];
                  [self removeFromParentViewController];
              } else {
                  
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
          }];
    
}

/**
 *  Cancel button was clicked, dismiss the popup
 *
 *  @param sender <#sender description#>
 */
- (void)onCancelButtonClicked:(id)sender
{
    [_timer invalidate];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

/**
 *  Function for showing the current countdown timer
 *
 *  @param timer Timer object
 */
- (void)timerFired:(NSTimer *)timer
{
    _timerCount = _timerCount - 1;
    if (_timerCount > 0) {
        _timeLabel.text = [NSString stringWithFormat:@"%i", _timerCount];
    } else {
        [timer invalidate];
        [_cancelButton setUserInteractionEnabled:NO];
        [_locationManager startUpdatingLocation];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
