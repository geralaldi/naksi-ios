//
//  ReminderPopupViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/9/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "ReminderPopupViewController.h"

@interface ReminderPopupViewController ()

@end


@implementation ReminderPopupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createInputAccessoryView];
    [_minuteTextField setInputAccessoryView:_inputAccessoryView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

/**
 *  Keyboard was shown, move the views up
 */
- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint activeTextFieldOrigin = _minuteTextField.frame.origin;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height = _insideView.frame.size.height - (keyboardSize.height - ((_outsideView.frame.size.height - _insideView.frame.size.height)/2));
    
    if (!CGRectContainsPoint(visibleRect, activeTextFieldOrigin)) {
        CGPoint scrollPoint = CGPointMake(0.0, activeTextFieldOrigin.y - visibleRect.size.height + 40);
        [_scrollView setContentOffset:scrollPoint animated:NO];
    }
}

/**
 *  Keyboard is hiding, reset the views position
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    [_scrollView setContentOffset:CGPointZero];
}

/**
 *  Create done button in keyboard, so user can dismiss the keyboard with it
 */
- (void)createInputAccessoryView
{
    if (!_inputAccessoryView) {
        _inputAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        _inputAccessoryView.backgroundColor = [UIColor whiteColor];
        _inputAccessoryView.alpha = 1;
        
        UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
        border.backgroundColor = [UIColor blackColor];
        
        UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        doneButton.frame = CGRectMake(255, 5, 60, 30.0);
        [doneButton setTitle: @"Done" forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [doneButton addTarget:self action:@selector(dismissKeyboard:)
             forControlEvents:UIControlEventTouchUpInside];
        [_inputAccessoryView addSubview:border];
        [_inputAccessoryView addSubview:doneButton];
    }
}

- (void)dismissKeyboard:(id)sender
{
    [_minuteTextField resignFirstResponder];
}

/**
 *  OK button clicked, show Settings screen, and set the expected time as the value from minuteTextField
 *
 *  @param sender Sender
 */
- (IBAction)onOKClicked:(id)sender {
    SettingsViewController *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    settingsViewController.segueIdentifier = @"Riding";
    settingsViewController.expectedTime = [_minuteTextField.text intValue];
    [self presentViewController:settingsViewController animated:YES completion:^{
        [self removeFromParentViewController];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
