//
//  PickContactViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "PickContactViewController.h"

@interface PickContactViewController ()

@end

@implementation PickContactViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _contactArray = [[NSMutableArray alloc] init];
    _contactTable.dataSource = self;
    _contactTable.delegate = self;
    _contactTable.allowsMultipleSelection = YES;
    _contactSearchBar.delegate = self;
    _angelCount = 0;
    
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
    _twitterUsernameLabel.text = [NSString stringWithFormat:@"@%@",[[FHSTwitterEngine sharedEngine] authenticatedUsername]];
    [self getContacts];
    
    self.searchDisplayController.searchResultsDelegate = self;
    self.searchDisplayController.searchResultsDataSource = self;
    self.searchDisplayController.searchResultsTableView.allowsMultipleSelection = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Pick Contact Screen";
}

/**
 *  Number of rows in one table section, if tableView is not regular table,
 *  then its number is the length of Search Contact Array, otherwise it is
 *  Contact Array's
 *
 *  @param tableView TableView object
 *  @param section   Section number
 *
 *  @return Number of rows
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != _contactTable) {
        return [_searchContactArray count];
    }
    return [_contactArray count];
}

/**
 *  Create cell for each indexpath/row
 *
 *  @param tableView TableView used
 *  @param indexPath Index of row
 *
 *  @return ContactTableCell instance
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactTableCell *cell = [_contactTable dequeueReusableCellWithIdentifier:@"ContactCell"];
    
    if (cell == nil) {
        cell = [[ContactTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ContactCell"];
    }
    
    ContactAngel *contactAngel = nil;
    
    if (tableView != _contactTable) {
        contactAngel = [_searchContactArray objectAtIndex:indexPath.row];
    } else {
        contactAngel = [_contactArray objectAtIndex:indexPath.row];
    }
    
    cell.contactNameLabel.text = contactAngel.contactName;
    cell.contactNumberLabel.text = contactAngel.contactNumber;
    if (contactAngel.isClicked) {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        _angelCount = _angelCount++;
    }
    return cell;
}

/**
 *  When searching, table shown should be reloaded based on the search string
 *
 *  @param controller   UISearchDisplayController object
 *  @param searchString Input string
 *
 *  @return Whether the table should be reloaded
 */
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

/**
 *  Search text based on input in contactArray's name, with format contactName contains the search string
 *
 *  @param searchText The search string
 *  @param scope      Scope
 */
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"contactName contains[c] %@", searchText];
    _searchContactArray = [NSMutableArray arrayWithArray:[_contactArray filteredArrayUsingPredicate:resultPredicate]];
}

/**
 *  We have to tell each cell's height in search table
 *
 *  @param tableView TableView used
 *  @param indexPath Index of row
 *
 *  @return Cell's height
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 62;
}

/**
 *  Get user contacts that has phone numbers, because we need it, save it contactArray
 */
- (void)getContacts
{
    CFErrorRef error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, &error);
    __block BOOL accessGranted = NO;
    
    /**
     *  Check if access to Contacts is permitted
     */
    if (ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    } else {
        accessGranted = YES;
    }
    
    if (accessGranted)  {
        if (addressBook != nil) {
            CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
            CFIndex numberOfPeople = CFArrayGetCount(people);
            NSUInteger i = 0;
            for (i=0; i < numberOfPeople;i++)  {
                ABRecordRef contactPerson = CFArrayGetValueAtIndex(people, i);
                ABMultiValueRef phones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
                if (ABMultiValueGetCount(phones) > 0) {
                    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
                    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
                    
                    CFIndex phonesCount = ABMultiValueGetCount(phones);
                    
                    for (CFIndex j=0 ; j < phonesCount ; j++) {
                        ContactAngel *angel = [[ContactAngel alloc] init];
                        angel.contactName = [NSString stringWithFormat:@"%@ %@",
                                             (firstName != nil) ? firstName : @"",
                                              (lastName != nil) ? lastName : @""];
                        angel.contactNumber = [NSString stringWithFormat:@"%@",
                                                (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex
                                                (phones, j)];
                        [_contactArray addObject:angel];
                    }
                }
            }
            
            AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
            NSMutableArray *tempArray = [appDelegate getContactArray];
            if ([tempArray count] != [_contactArray count]) {
                
            } else {
                _contactArray = tempArray;
            }
            
            NSArray *sortedArray = [_contactArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                NSString *first = ((ContactAngel *)obj1).contactName;
                NSString *second = ((ContactAngel *)obj2).contactName;
                return [first compare:second];
            }];
            
            _contactArray = [[NSMutableArray alloc] initWithArray:sortedArray];
            for (int i=0 ; i < [_contactArray count] ; i++) {
                [[_contactArray objectAtIndex:i] setListPosition:i];
            }
            [_contactTable reloadData];
        }
    }
}

/**
 *  When selecting contact, change the checkbox image, and set the contact's clicked property to YES
 *  and when the selected contact is more than 3, show and alert to notify user that they can only
 *  choose 3 contacts
 *
 *  @param tableView TableView object
 *  @param indexPath Index of row selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != _contactTable) {
        if (_angelCount < 3) {
            ContactAngel *contactAngel = [_searchContactArray objectAtIndex:indexPath.row];
            BOOL selected = contactAngel.clicked;
            [[_searchContactArray objectAtIndex:indexPath.row] setClicked:!selected];
            [[_contactArray objectAtIndex:contactAngel.listPosition] setClicked:!selected];
            _angelCount = _angelCount + 1;
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"We will only send message to up to 3 contact number" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            [alertView show];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        [_contactTable reloadData];
    } else {
        if (_angelCount < 3) {
            NSInteger row = indexPath.row;
            ContactAngel *contactAngel = [_contactArray objectAtIndex:row];
            BOOL selected = contactAngel.clicked;
            [[_contactArray objectAtIndex:row] setClicked:!selected];
            _angelCount = _angelCount + 1;
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"We will only send message to up to 3 contact number" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            [alertView show];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }
}

/**
 *  When deselect a contact, change the checkbox image, and set the contact's clicked property to NO
 *
 *  @param tableView TableView object
 *  @param indexPath Index of row selected
 */
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_angelCount > 0)
        _angelCount = _angelCount - 1;
    NSInteger row = indexPath.row;
    if (tableView != _contactTable) {
        ContactAngel *contactAngel = [_searchContactArray objectAtIndex:row];
        BOOL selected = contactAngel.clicked;
        [[_contactArray objectAtIndex:contactAngel.listPosition] setClicked:!selected];
    } else {
        ContactAngel *contactAngel = [_contactArray objectAtIndex:row];
        BOOL selected = contactAngel.clicked;
        [[_contactArray objectAtIndex:row] setClicked:!selected];
    }
    [_contactTable reloadData];
}

/**
 *  Next button clicked, save the selected contacts in App Delegate, then show Pick Twitter Screen
 *
 *  @param sender Sender
 */
- (IBAction)onNextClicked:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i < _contactArray.count ; i++) {
        ContactAngel *angel = [_contactArray objectAtIndex:i];
        if (angel.clicked) {
            [tempArray addObject:angel];
        }
    }
    
    [appDelegate setContactArray:_contactArray];
    [appDelegate setSelectedContactArray:tempArray];
    
    [self performSegueWithIdentifier:@"PickTwitter" sender:self];
}

/**
 *  Save button clicked, this is when changing the contact angels, save the selected to App Delegate
 *  when done, dismiss the screen and update the angel table in Settings screen
 *
 *  @param sender Sender
 */
- (IBAction)onSaveClicked:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate setContactArray:_contactArray];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i < _contactArray.count ; i++) {
        ContactAngel *angel = [_contactArray objectAtIndex:i];
        if (angel.clicked) {
            [tempArray addObject:angel];
        }
    }
    [appDelegate setSelectedContactArray:tempArray];
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate updateTable];
    }];
}

/**
 *  When user begins to search in iOS 7 devices, we have to move the search bar up
 *  so the views behind the search bar are visible
 *
 *  @param controller SearchDisplayController
 */
- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    controller.searchResultsDataSource = self;
    controller.searchResultsDelegate = self;
    controller.searchResultsTableView.delegate = self;
    controller.searchResultsTableView.allowsMultipleSelection = YES;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        [UIView animateWithDuration:0.25 animations:^{
            _contactSearchBar.transform = CGAffineTransformMakeTranslation(0, -120);
        }];
    }
}

/**
 *  When the search is ending, return the search bar position to its default
 *
 *  @param controller SearchDisplayController
 */
- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        [UIView animateWithDuration:0.25 animations:^{
            _contactSearchBar.transform = CGAffineTransformIdentity;
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
