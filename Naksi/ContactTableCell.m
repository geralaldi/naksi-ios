//
//  ContactTableCell.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/6/14.
//  Copyright (c) 2014 Geraldi Kusuma Arnanto. All rights reserved.
//

#import "ContactTableCell.h"

@implementation ContactTableCell

/**
 *  Set checkbox image according to selection state
 *
 *  @param selected selection state
 *  @param animated with animation
 */
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected) {
        NSLog(@"selected");
        UIImage *image = [UIImage imageNamed:@"icon_selected_contact"];
        [_contactCheckBox setImage:image];
    }
    else {
        UIImage *image = [UIImage imageNamed:@"icon_notselected_contact"];
        [_contactCheckBox setImage:image];
    }
}

@end
