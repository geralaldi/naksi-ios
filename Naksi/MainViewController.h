//
//  MainViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/4/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
#import "GAI.h"
#import <CoreLocation/CoreLocation.h>

/**
 *  View Controller for Main Screen
 */

@interface MainViewController : GAITrackedViewController <UITextFieldDelegate, UIAlertViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager; ///< Location Manager object
@property (strong, nonatomic) CLLocation *bestLocation; ///< Latest location object
@property (strong, nonatomic) IBOutlet UILabel *userLabel; ///< Label for user twitter name
@property (strong, nonatomic) IBOutlet UITextField *taxiNumberField; ///< TextField for Taxi Number
@property (strong, nonatomic) IBOutlet UITextField *taxiCompanyField; ///< TextField for Taxi Company
@property (strong, nonatomic) IBOutlet UITextField *destinationField; ///< TextField for Destination
@property (strong, nonatomic) IBOutlet UITextField *durationField; ///< TextField for Duration
@property (strong, nonatomic) IBOutlet UIImageView *startButton; ///< ImageView for start button
@property (strong, nonatomic) IBOutlet UIImageView *twitterAvatar; ///< ImageView for profile picture
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView; ///< Scroll View
@property (strong, nonatomic) UIImage *profilePicture; ///< Image for profile picture
@property (assign, nonatomic) UITextField *activeTextField; ///< Selected TextField
@property (strong, nonatomic) UIAlertView *emptyAlertView; ///< AlertView for empty field
@property (strong, nonatomic) UIView *inputAccessoryView; ///< Keyboard Accessory View
@property double latitude; ///< Location latitude
@property double longitude; ///< Location longitude
@property BOOL locationSent; ///< Location sent status

- (IBAction)onStartClicked:(id)sender;
- (IBAction)onProfileClicked:(id)sender;

@end
