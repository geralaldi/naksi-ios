//
//  FinishPopupViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/9/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FinishedViewController.h"
#import "ReminderPopupViewController.h"

/**
 *  View Controller for Finish Dialog Popup
 */

@interface FinishPopupViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *insideView; ///< View of the main popup
@property (strong, nonatomic) IBOutlet UIView *outsideView; ///< View of the container

- (IBAction)onYesClicked:(id)sender;
- (IBAction)onNoClicked:(id)sender;
@end
