//
//  PopupSegue.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/9/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FinishPopupViewController.h"
#import "SOSPopupViewController.h"

/**
 *  Segue for Popup screens
 */
@interface PopupSegue : UIStoryboardSegue

@end
