//
//  SplashViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

/**
 *  View controller for Splash screen
 */
@interface SplashViewController : GAITrackedViewController

@property (nonatomic, strong) NSTimer *timer;

@end
