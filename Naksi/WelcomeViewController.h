//
//  WelcomeViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
#import "GAI.h"

/**
 *  View controller for welcome screen
 */
@interface WelcomeViewController : GAITrackedViewController

@property (strong, nonatomic) IBOutlet UIImageView *twitterAvatar; ///< ImageView for profile picture
@property (strong, nonatomic) IBOutlet UILabel *twitterUsernameWelcome; ///< Label for twitter username
@property (strong, nonatomic) IBOutlet UIImageView *nextButton; ///< ImageView for next button
@property (strong, nonatomic) UIImage *profilePicture; ///< Image object for profile picture

- (IBAction)nextButtonClicked:(id)sender;

@end
