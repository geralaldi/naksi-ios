//
//  TwitterTableCell.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/20/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "TwitterTableCell.h"

@implementation TwitterTableCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        UIImage *image = [UIImage imageNamed:@"icon_selected_twitter"];
        [_twitterCheckBox setImage:image];
    } else {
        UIImage *image = [UIImage imageNamed:@"icon_notselected_twitter"];
        [_twitterCheckBox setImage:image];
    }
}

@end
