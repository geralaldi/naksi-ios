//
//  TwitterAngel.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 2/26/14.
//  Copyright (c) 2014 Geraldi Kusuma Arnanto. All rights reserved.
//

#import "TwitterAngel.h"

@implementation TwitterAngel

- (id)initWithValue:(NSString *)twitterName :(NSString *)twitterID :(BOOL)clicked {
    _twitterName = twitterName;
    _twitterId = twitterID;
    _clicked = clicked;
    
    return self;
}

/**
 *  Method for decoding object from file
 *
 *  @param aDecoder Object Decoder
 *
 *  @return id
 */
- (id)initWithCoder:(NSCoder *)aDecoder
{
    _twitterId = [aDecoder decodeObjectForKey:@"twitterId"];
    _twitterName = [aDecoder decodeObjectForKey:@"twitterName"];
    _clicked = [aDecoder decodeBoolForKey:@"clicked"];
    _listPosition = [aDecoder decodeIntegerForKey:@"listPosition"];
    return self;
}

/**
 *  Method for encoding object into file
 *
 *  @param coder Object encoder
 */
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeBool:_clicked forKey:@"clicked"];
    [aCoder encodeObject:_twitterId forKey:@"twitterId"];
    [aCoder encodeObject:_twitterName forKey:@"twitterName"];
    [aCoder encodeInteger:_listPosition forKey:@"listPosition"];
}

/**
 *  Getter for clicked status of TwitterAngel instance
 *
 *  @return clicked status
 */
- (BOOL)isClicked {
    return _clicked;
}

/**
 *  Setter for clicked status of TwitterAngel instance
 *
 *  @param clicked status
 */
-(void)setClicked:(BOOL)clicked
{
    _clicked = clicked;
}

@end
