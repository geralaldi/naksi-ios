//
//  ContainerViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SummaryChildViewController.h"
#import "SavedChildViewController.h"
#import "RidingChildViewController.h"

/**
 *  Delegate of Container View Controller
 */
@protocol ContainerDelegate <NSObject>

- (void)saveClicked;
- (void)isRiding;
- (void)showFinishDialogOnSettings;
- (void)showSOSDialogOnSettings;
- (void)enableEdit;

@end

/**
 *  Container View Controller for containing child view controller in Settings View
 */
@interface ContainerViewController : UIViewController <SummaryDelegate, RidingDelegate, SavedDelegate>

@property NSInteger expectedTime; // expected time of arrival in minutes
@property (weak, nonatomic) id <ContainerDelegate> delegate; // delegate object
@property (strong, nonatomic) NSString *currentSegueIdentifier; // current segue identifier
@property (strong, nonatomic) SummaryChildViewController *summaryViewController; // summary view controller instance
@property (strong, nonatomic) SavedChildViewController *savedViewController; // saved view controller instance
@property (strong, nonatomic) RidingChildViewController *ridingViewController; // riding view controller instance

@end
