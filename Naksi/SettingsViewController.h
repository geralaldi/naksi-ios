//
//  SettingsViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/19/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AngelTableCell.h"
#import "AFNetworking.h"
#import "ContactAngel.h"
#import "TwitterAngel.h"
#import "AppDelegate.h"
#import "PickContactViewController.h"
#import "PickTwitterViewController.h"
#import "ContainerViewController.h"
#import "GAI.h"

@protocol SettingsDelegate <NSObject>

- (void)updateTravelDuration;

@end

/**
 *  View controller for Settings view, the biggest and most complicated class in this project
 *  pfft
 */
@interface SettingsViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, PickContactDelegate, PickTwitterDelegate, ContainerDelegate>

@property NSInteger contactCount; ///< Contact angels count
@property NSInteger twitterCount; ///< Twitter angels count
@property NSInteger expectedTime; ///< Expected time of arrival

@property (strong, nonatomic) id <SettingsDelegate> delegate; ///< Delegate object
@property (strong, nonatomic) NSString *segueIdentifier; ///< Segue identifier to be used for child view
@property (strong, nonatomic) NSMutableArray *contactArray; ///< Array of contact angels
@property (strong, nonatomic) NSMutableArray *twitterArray; ///< Array of twitter angels
@property (strong, nonatomic) NSMutableString *contactNames; ///< String to store contact angels names
@property (strong, nonatomic) NSMutableString *twitterNames; ///< String to store twitter angels names
@property (strong, nonatomic) IBOutlet UILabel *twitterName; ///< Label for twitter username
@property (strong, nonatomic) IBOutlet UITableView *angelTable; ///< Angels table
@property (strong, nonatomic) IBOutlet UIImageView *twitterAvatar; ///< ImageView for profile picture
@property (strong, nonatomic) IBOutlet UIView *containerView; ///< Container view for child views
@property (strong, nonatomic) UIImage *profilePicture; ///< Image object for profile picture

@end
