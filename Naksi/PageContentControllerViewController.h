//
//  PageContentControllerViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/2/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
#import "AFNetworking.h"

@interface PageContentControllerViewController : UIViewController
{
    BOOL isLastPage;
}

@property (strong, nonatomic) IBOutlet UIImageView *tourPageBackground;
@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIImageView *loginButton;

@property NSUInteger pageIndex;
@property (strong, nonatomic) NSString *imageFile;

- (IBAction)loginToTwitter:(id)sender;

@end
