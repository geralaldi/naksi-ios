//
//  AngelTableCell.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/27/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Subclass of UITableViewCell to be used for Settings View Angel Table
 */

@interface AngelTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *angelName;
@property (strong, nonatomic) IBOutlet UILabel *angelCount;
@property (strong, nonatomic) IBOutlet UIImageView *angelCountFrame;

@end
