//
//  MainViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/4/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "MainViewController.h"
#import "SettingsViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _taxiNumberField.delegate = self;
    _taxiCompanyField.delegate = self;
    _destinationField.delegate = self;
    _durationField.delegate = self;
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    _emptyAlertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You have to fill all forms" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
    _userLabel.text = [NSString stringWithFormat:@"Welcome, @%@", [[FHSTwitterEngine sharedEngine] authenticatedUsername]];
    
    /**
     *  Keyboard Notification
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    /**
     *  Single tap gesture recognizer for Image Views
     */
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onStartClicked:)];
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onProfileClicked:)];
    
    [singleTap setNumberOfTapsRequired:1];
    [profileTap setNumberOfTapsRequired:1];
    [_startButton addGestureRecognizer:singleTap];
    [_twitterAvatar addGestureRecognizer:profileTap];
    
    /**
     *  Get Profile picture in the background, then set it to twitterAvatar
     */
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self getProfilePicture];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                @autoreleasepool {
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [_twitterAvatar setImage:_profilePicture];
                }
            });
        }
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Main Screen";
}

/**
 *  Keyboard appeared, scroll the views up
 */
- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    CGPoint activeTextFieldOrigin = _durationField.frame.origin;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height = self.view.frame.size.height- keyboardSize.height - 30;
    
    if (!CGRectContainsPoint(visibleRect, activeTextFieldOrigin)) {
        CGPoint scrollPoint = CGPointMake(0.0, activeTextFieldOrigin.y - visibleRect.size.height + 20);
        [_scrollView setContentOffset:scrollPoint animated:NO];
    }
}

/**
 *  Keyboard is hiding, reset the views position
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    [_scrollView setContentOffset:CGPointZero];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeTextField = textField;
    [self createInputAccessoryView];
    [textField setInputAccessoryView:_inputAccessoryView];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeTextField = nil;
}

/**
 *  Add done button to keyboard
 */
- (void)createInputAccessoryView
{
    _inputAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    _inputAccessoryView.backgroundColor = [UIColor whiteColor];
    _inputAccessoryView.alpha = 1;
    
    UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    border.backgroundColor = [UIColor blackColor];
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(255, 5, 60, 30.0);
    [doneButton setTitle: @"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(dismissKeyboard:)
         forControlEvents:UIControlEventTouchUpInside];
    [_inputAccessoryView addSubview:border];
    [_inputAccessoryView addSubview:doneButton];
}

- (void)dismissKeyboard:(id)sender
{
    [_activeTextField resignFirstResponder];
}

/**
 *  Start button clicked, check if text fields are not empty, get location, save taxi info, then show Riding Screen
 *  otherwise, show alert view to notify user that all fields cannot be empty
 */
- (void)onStartClicked:(id)sender
{
    if ([self checkInput]) {
        [_locationManager startUpdatingLocation];
        [[NSUserDefaults standardUserDefaults] setObject:_taxiCompanyField.text forKey:@"taxiCompany"];
        [[NSUserDefaults standardUserDefaults] setObject:_taxiNumberField.text forKey:@"taxiNumber"];
        [self performSegueWithIdentifier:@"Riding" sender:self];
    } else {
        [_emptyAlertView show];
    }
}

/**
 *  Profile picture is clicked, show Settings Screen
 */
- (void)onProfileClicked:(id)sender
{
    SettingsViewController *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    settingsViewController.segueIdentifier = @"Summary";
    [self presentViewController:settingsViewController animated:YES completion:nil];
}

/**
 *  Location updated
 *
 *  @param manager   Location Manager object
 *  @param locations Location Array
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = (CLLocation *) [locations lastObject];
    NSTimeInterval locationAge = -[currentLocation.timestamp timeIntervalSinceNow];
    
    /**
     *  If location obtained in more than 5 seconds ago or its accuracy is less than zero, exit
     */
    if (locationAge > 5.0) return;
    if (currentLocation.horizontalAccuracy < 0) return;
    
    /**
     *  If latest location is empty or its accuracy is more than current location's,
     *  then assign current location as latest location
     */
    if (_bestLocation == nil || _bestLocation.horizontalAccuracy > currentLocation.horizontalAccuracy) {
        _bestLocation = currentLocation;
        double longitude = _bestLocation.coordinate.longitude;
        double latitude = _bestLocation.coordinate.latitude;
        
        /**
         *  If current location's accuracy is less than 100 meters, then send the location to server
         *  and stop updating location, otherwise start a timer that will fire 5 seconds from now
         */
        if (currentLocation.horizontalAccuracy <= 100) {
            [self sendToServer:longitude and:latitude];
            [_locationManager stopUpdatingLocation];
        } else {
            if (!_locationSent)
                [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(sendLocation:) userInfo:nil repeats:NO];
            _locationSent = YES;
        }
    }
}

/**
 *  Error occured when getting location, show an alert view to notify the user
 *
 *  @param manager Location Manager object
 *  @param error   Error message
 */
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"Location Error" message:@"Error when retrieving your location" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
    NSLog(@"%@", error);
}

/**
 *  Executed when the timer is fired, send location to server
 *
 *  @param sender Timer
 */
- (void)sendLocation:(id)sender
{
    _locationSent = YES;
    [self sendToServer:_bestLocation.coordinate.longitude and:_bestLocation.coordinate.latitude];
    [_locationManager stopUpdatingLocation];
}

/**
 *  Send location, travel info to server
 *
 *  @param longitude Location longitude
 *  @param latitude  Location latitude
 */
- (void)sendToServer:(double)longitude and:(double)latitude
{
    NSInteger expectedDuration = [_durationField.text intValue] * 1000 * 60;
    NSDictionary *travelDictionary = [[NSDictionary alloc] init];
    travelDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSNumber numberWithInteger:expectedDuration], @"expected_time", [NSNumber numberWithDouble:_latitude],
                        @"location_lat", [NSNumber numberWithDouble:_longitude],
                        @"location_long", _destinationField.text,
                        @"info", _taxiCompanyField.text,
                        @"taxi_company", _taxiNumberField.text, @"taxi_number", nil];
    
    NSString *urlString = [NSString stringWithFormat:@"http://devel.wejoin.us/naksi/index.php/api/travel/depart?api_key=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userToken"]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:urlString parameters:travelDictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *) responseObject;
              BOOL status = (BOOL) [response objectForKey:@"status"];
              if (status) {
                  
              } else {
                  
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
          }];
}

/**
 *  Check whether any Text Fields is empty
 *
 *  @return Whether any Text Field is empty
 */
- (BOOL)checkInput
{
    if (_taxiCompanyField.text.length > 0 && _taxiNumberField.text.length > 0 && _destinationField.text.length > 0 && _durationField.text.length > 0) {
        return YES;
    }
    return NO;
}

/**
 *  Get profile picture from Twitter, retry when failed
 */
- (void)getProfilePicture
{
    if ([[FHSTwitterEngine sharedEngine] isAuthorized]) {
        NSString *username = FHSTwitterEngine.sharedEngine.authenticatedUsername;
        _profilePicture = [[FHSTwitterEngine sharedEngine] getProfileImageForUsername:username andSize:FHSTwitterEngineImageSizeOriginal];
        if ([_profilePicture isKindOfClass:[NSError class]]) {
            [self getProfilePicture];
        }
    } else {
        NSLog(@"Not authorized");
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

/**
 *  Preparing for segue, SettingsViewController's segue identifier as Riding and expected time based on Duration Field value
 *
 *  @param segue  Segue object
 *  @param sender Sender object
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    SettingsViewController *settingsViewController = (SettingsViewController *) segue.destinationViewController;
    settingsViewController.segueIdentifier = @"Riding";
    settingsViewController.expectedTime = [_durationField.text integerValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
