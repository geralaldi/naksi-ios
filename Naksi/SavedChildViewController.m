//
//  SavedChildViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "SavedChildViewController.h"

@interface SavedChildViewController ()

@end

@implementation SavedChildViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onStartClicked:)];
    UITapGestureRecognizer *changeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onChangeSettingsClicked:)];
    
    [singleTap setNumberOfTapsRequired:1];
    [changeTap setNumberOfTapsRequired:1];
    [_startButton addGestureRecognizer:singleTap];
    [_changeSettingsButton addGestureRecognizer:changeTap];
}

/**
 *  Change settings button was clicked, send delegate to execute changeSettings
 *
 *  @param sender Sender
 */
- (void)onChangeSettingsClicked:(id)sender
{
    [_delegate changeSettings];
}

/**
 *  Start button was clicked, show Main screen
 *
 *  @param sender Sender
 */
- (void)onStartClicked:(id)sender
{
    MainViewController *mainViewController = (MainViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [self presentViewController:mainViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
