//
//  PickContactViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import "ContactAngel.h"
#import "ContactTableCell.h"
#import "AppDelegate.h"
#import "GAI.h"

@protocol PickContactDelegate <NSObject>

- (void)updateTable;

@end

/**
 *  Controller for Pick Contact View
 */

@interface PickContactViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property (weak, nonatomic) id <PickContactDelegate> delegate; ///< Pick Contact Delegate Object

@property int angelCount; ///< Numbers of selected contact
@property (strong, nonatomic) NSMutableArray *contactArray; ///< Array of Contact Angels
@property (strong, nonatomic) NSMutableArray *searchContactArray; ///< Temporary array of Contact Angels when searching
@property (strong, nonatomic) IBOutlet UILabel *twitterUsernameLabel; ///< Label for twitter username
@property (strong, nonatomic) IBOutlet UITableView *contactTable; ///< Table View for user's contacts
@property (strong, nonatomic) IBOutlet UISearchBar *contactSearchBar; ///< Search Bar
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar; ///< Navigation Bar that contains Save Button

- (IBAction)onNextClicked:(id)sender;
- (IBAction)onSaveClicked:(id)sender;

@end
