//
//  RidingChildViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FinishedViewController.h"

@protocol RidingDelegate <NSObject>

- (void)showFinishDialogOnContainer;
- (void)showSOSDialogOnContainer;

@end

/**
 *  View controller for riding child view
 */
@interface RidingChildViewController : UIViewController <UIAlertViewDelegate>

@property (retain, nonatomic) id <RidingDelegate> delegate; ///< Delegate object
@property (strong, nonatomic) UIAlertView *arriveAlert; ///< Alertview for notifying user
@property (strong, nonatomic) IBOutlet UILabel *arrivalTimeLabel; ///< Label for arrival message send time
@property (strong, nonatomic) IBOutlet UILabel *estimatedTimeLabel; ///< Label for ETA
@property (strong, nonatomic) IBOutlet UIImageView *finishButton; ///< ImageView for finish button
@property (strong, nonatomic) IBOutlet UIImageView *sosButton; ///< ImageView for SOS button
@property NSInteger expectedTime; ///< expected time of arrival
@property NSInteger expectedTimeInSeconds; ///< expected time of arrival in seconds

- (IBAction)onFinishClicked:(id)sender;
- (IBAction)onSOSClicked:(id)sender;

@end
