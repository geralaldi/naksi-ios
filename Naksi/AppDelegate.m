//
//  AppDelegate.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "WelcomeViewController.h"

@implementation AppDelegate

/**
 *  Execute functions when app finished launching
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [[FHSTwitterEngine sharedEngine] permanentlySetConsumerKey:@"J9orbdXWlsO1V5CcQ4c2Eg" andSecret:@"OJecUdnGO1ODTODFRgmniIU4mSva9M5g0SYEFykY"];
    [[FHSTwitterEngine sharedEngine] setDelegate:self];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-48942324-1"];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
    _paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if (_paths.count > 0) {
        _contactArrayPath = [[_paths objectAtIndex:0] stringByAppendingPathComponent:@"contact.out"];
        _twitterArrayPath = [[_paths objectAtIndex:0] stringByAppendingPathComponent:@"twitter.out"];
        _selectedContactArrayPath = [[_paths objectAtIndex:0] stringByAppendingPathComponent:@"SelectedContact.out"];
        _selectedTwitterArrayPath = [[_paths objectAtIndex:0] stringByAppendingPathComponent:@"SelectedTwitter.out"];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:_contactArrayPath]) {
        [[NSFileManager defaultManager] createFileAtPath:_contactArrayPath contents:nil attributes:nil];
    }

    if (![[NSFileManager defaultManager] fileExistsAtPath:_twitterArrayPath]) {
        [[NSFileManager defaultManager] createFileAtPath:_twitterArrayPath contents:nil attributes:nil];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:_selectedContactArrayPath]) {
        [[NSFileManager defaultManager] createFileAtPath:_selectedContactArrayPath contents:nil attributes:nil];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:_selectedTwitterArrayPath]) {
        [[NSFileManager defaultManager] createFileAtPath:_selectedTwitterArrayPath contents:nil attributes:nil];
    }
    
    return YES;
}

- (void)storeAccessToken:(NSString *)accessToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessToken forKey:@"userAccessToken"];
    [defaults synchronize];
}

- (NSString *)loadAccessToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults objectForKey:@"userAccessToken"];
    return accessToken;
}

- (NSMutableArray *)getContactArray
{
    if (_contactArray == nil) {
        _contactArray = [[NSMutableArray alloc] init];
        _contactArray = [NSKeyedUnarchiver unarchiveObjectWithFile:_contactArrayPath];
    }
    return _contactArray;
}

- (void)setContactArray:(NSMutableArray *)contactArray
{
    _contactArray = contactArray;
}

- (NSMutableArray *)getSelectedContactArray
{
    if (_contactArray == nil) {
        _selectedContactArray = [[NSMutableArray alloc] init];
        _selectedContactArray = [NSKeyedUnarchiver unarchiveObjectWithFile:_selectedContactArrayPath];
    }
    return _selectedContactArray;
}

- (void)setSelectedContactArray:(NSMutableArray *)contactArray
{
    _selectedContactArray = contactArray;
}

- (NSMutableArray *)getTwitterArray
{
    if (_twitterArray == nil) {
        _twitterArray = [[NSMutableArray alloc] init];
        _twitterArray = [NSKeyedUnarchiver unarchiveObjectWithFile:_twitterArrayPath];
    }
    return _twitterArray;
}

- (void)setTwitterArray:(NSMutableArray *)twitterArray
{
    _twitterArray = twitterArray;
}

- (NSMutableArray *)getSelectedTwitterArray
{
    if (_twitterArray == nil) {
        _selectedTwitterArray = [[NSMutableArray alloc] init];
        _selectedTwitterArray = [NSKeyedUnarchiver unarchiveObjectWithFile:_selectedTwitterArrayPath];
    }
    return _selectedTwitterArray;
}

- (void)setSelectedTwitterArray:(NSMutableArray *)twitterArray
{
    _selectedTwitterArray = twitterArray;
}

/**
 *  Save arrays to files
 */
- (void)saveDataToDisk
{
    [NSKeyedArchiver archiveRootObject:_selectedTwitterArray toFile:_selectedTwitterArrayPath];
    [NSKeyedArchiver archiveRootObject:_twitterArray toFile:_twitterArrayPath];
    [NSKeyedArchiver archiveRootObject:_selectedContactArray toFile:_selectedContactArrayPath];
    [NSKeyedArchiver archiveRootObject:_contactArray toFile:_contactArrayPath];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    [FBSDKSettings setDefaultAppID:@"1474906246071375"];
//    [FBAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
