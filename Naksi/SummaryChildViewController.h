//
//  SummaryChildViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SummaryDelegate <NSObject>

- (void)saveClicked;

@end

/**
 *  View controller for summary child view
 */
@interface SummaryChildViewController : UIViewController

@property (weak, nonatomic) id <SummaryDelegate> delegate; ///< Delegate object
@property (strong, nonatomic) IBOutlet UIImageView *saveButton; ///< ImageView for save button

- (IBAction)onSaveClicked:(id)sender;

@end
