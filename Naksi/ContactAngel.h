//
//  ContactAngel.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 2/26/14.
//  Copyright (c) 2014 Geraldi Kusuma Arnanto. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class for Contact Angel Object
 */

@interface ContactAngel : NSObject <NSCoding>

@property NSInteger listPosition;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, assign) BOOL clicked;

- (id)initWithValue:(NSString *)contactName :(NSString *)contactNumber :(BOOL)clicked;
- (BOOL) isClicked;
- (void) setClicked:(BOOL)clicked;

@end
