//
//  FinishedViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/7/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
#import "AFNetworking.h"
#import "GAI.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>


/**
 *  Controller for Finished View
 */
@interface FinishedViewController : GAITrackedViewController <UITextViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager; ///< Location Manager object
@property (strong, nonatomic) CLLocation *bestLocation; ///< Latest Location object
@property (strong, nonatomic) UIImage *profilePicture; ///< Image for Profile Picture
@property (strong, nonatomic) IBOutlet UIImageView *twitterAvatar; ///< Image View for Profile Picture
@property (strong, nonatomic) IBOutlet UIImageView *starOne; ///< Image View for One Star
@property (strong, nonatomic) IBOutlet UIImageView *starTwo; ///< Image View for Two Star
@property (strong, nonatomic) IBOutlet UIImageView *starThree; ///< Image View for Three Star
@property (strong, nonatomic) IBOutlet UIImageView *starFour; ///< Image View for Four Star
@property (strong, nonatomic) IBOutlet UIImageView *starFive; ///< Image View for Five Star
@property (strong, nonatomic) IBOutlet UILabel *twitterUsername; ///< Label for Twitter Username
@property (strong, nonatomic) IBOutlet UITextView *reviewTextView; ///< TextView for Review
@property (strong, nonatomic) IBOutlet UIImageView *doneButton; ///< ImageView for Done Button
@property (strong, nonatomic) IBOutlet UIImageView *skipButton; ///< ImageView for Skip button
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView; ///< ScrollView object
@property (strong, nonatomic) UIView *inputAccessoryView; ///< View for Accessory in Keyboard
@property int locationTimerCount; ///< Location Timeout Count
@property BOOL locationSent; ///< Location sent status

- (IBAction)onDoneClicked:(id)sender;
- (IBAction)onSkipClicked:(id)sender;

- (IBAction)oneStarSelected:(id)sender;
- (IBAction)twoStarSelected:(id)sender;
- (IBAction)threeStarSelected:(id)sender;
- (IBAction)fourStarSelected:(id)sender;
- (IBAction)fiveStarSelected:(id)sender;

@end
