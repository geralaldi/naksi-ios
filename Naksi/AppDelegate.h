//
//  AppDelegate.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FHSTwitterEngine/FHSTwitterEngine.h>
#import <GoogleAnalytics/GAI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, FHSTwitterEngineAccessTokenDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *contactArray; ///< Array of contacts
@property (strong, nonatomic) NSMutableArray *selectedContactArray; ///< Array of selected contacts
@property (strong, nonatomic) NSMutableArray *twitterArray; ///< Array of twitter followers
@property (strong, nonatomic) NSMutableArray *selectedTwitterArray; ///< Array of selected twitter followers
@property (strong, nonatomic) NSString *contactArrayPath; ///< Path to contact array file
@property (strong, nonatomic) NSString *selectedContactArrayPath; ///< Path to selected contact array file
@property (strong, nonatomic) NSString *twitterArrayPath; ///< Path to twitter array file
@property (strong, nonatomic) NSString *selectedTwitterArrayPath; ///< Path to selected twitter array file
@property (strong, nonatomic) NSArray *paths; ///< Paths available to App

- (NSMutableArray *)getContactArray;
- (NSMutableArray *)getSelectedContactArray;
- (NSMutableArray *)getTwitterArray;
- (NSMutableArray *)getSelectedTwitterArray;
- (void)setTwitterArray:(NSMutableArray *)twitterArray;
- (void)setSelectedTwitterArray:(NSMutableArray *)twitterArray;
- (void)setContactArray:(NSMutableArray *)contactArray;
- (void)setSelectedContactArray:(NSMutableArray *)contactArray;
- (void)saveDataToDisk;

@end
