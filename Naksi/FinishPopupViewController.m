//
//  FinishPopupViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/9/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "FinishPopupViewController.h"

@interface FinishPopupViewController ()

@end

@implementation FinishPopupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  Yes Button is clicked, show Finished Screen
 *
 *  @param sender Selector
 */
- (IBAction)onYesClicked:(id)sender {
    FinishedViewController *finishedViewController = (FinishedViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"FinishedViewController"];
    [self presentViewController:finishedViewController animated:YES completion:nil];
}

/**
 *  No Button is clicked, show Reminder Popup
 *
 *  @param sender Selector
 */
- (IBAction)onNoClicked:(id)sender {
    [self performSegueWithIdentifier:@"Reminder" sender:self];
}

@end
