//
//  TwitterAngel.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 2/26/14.
//  Copyright (c) 2014 Geraldi Kusuma Arnanto. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class for Twitter angel object
 */
@interface TwitterAngel : NSObject <NSCoding>

@property NSInteger listPosition;
@property (nonatomic, strong) NSString *twitterName;
@property (nonatomic, strong) NSString *twitterId;
@property (nonatomic, assign) BOOL clicked;

- (id)initWithValue:(NSString*)twitterName :(NSString*)twitterID :(BOOL)clicked;
- (BOOL) isClicked;
- (void) setClicked:(BOOL)clicked;

@end
