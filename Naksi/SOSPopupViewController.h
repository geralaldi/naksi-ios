//
//  SOSPopupViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/15/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
#import "AFNetworking.h"
#import <CoreLocation/CoreLocation.h>

/**
 *  View controller for SOS Popup screen
 */
@interface SOSPopupViewController : UIViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager; ///< Location Manager object
@property (strong, nonatomic) CLLocation *bestLocation; ///< Latest location object
@property (strong, nonatomic) IBOutlet UILabel *timeLabel; ///< Label for countdown time
@property (strong, nonatomic) IBOutlet UILabel *cancelButton; ///< Label for cancel button
@property (strong, nonatomic) IBOutlet UIView *insideView; ///< Popup main view
@property (strong, nonatomic) IBOutlet UIView *outsideView; ///< Popup container view
@property (strong, nonatomic) NSTimer *timer; ///< Timer to be used for firing send to server
@property NSInteger timerCount; ///< countdown timer
@property BOOL locationSent; ///< Location sent status

- (IBAction)onCancelButtonClicked:(id)sender;

@end
