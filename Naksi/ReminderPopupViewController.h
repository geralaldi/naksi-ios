//
//  ReminderPopupViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/9/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"

/**
 *  View controller for Reminder Popup
 */
@interface ReminderPopupViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *insideView; ///< Popup main view
@property (strong, nonatomic) IBOutlet UIView *outsideView; ///< Popup container
@property (strong, nonatomic) IBOutlet UITextField *minuteTextField; ///< TextField for minute of reminder
@property (strong, nonatomic) UIView *inputAccessoryView; ///< View for Keyboard accessory
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView; ///< Scroll View

- (IBAction)onOKClicked:(id)sender;

@end
