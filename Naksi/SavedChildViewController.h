//
//  SavedChildViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@protocol SavedDelegate <NSObject>

- (void)changeSettings;

@end

/**
 *  View controller for saved child view
 */
@interface SavedChildViewController : UIViewController

@property (retain, nonatomic) id <SavedDelegate> delegate; ///< Delegate object
@property (strong, nonatomic) IBOutlet UILabel *changeSettingsButton; ///< Label for change settings button
@property (strong, nonatomic) IBOutlet UIImageView *startButton; ///< ImageView for start button

- (IBAction)onStartClicked:(id)sender;
- (IBAction)onChangeSettingsClicked:(id)sender;

@end
