//
//  ChangeSegue.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/3/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Empty Segue for changing between child view controller
 */

@interface ChangeSegue : UIStoryboardSegue

@end
