//
//  PopupSegue.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/9/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "PopupSegue.h"

@implementation PopupSegue

/**
 *  Perform segue, and setup view based on the popup to be shown
 */
- (void)perform
{
    UIViewController *src = [self sourceViewController];
    
    if ([[self destinationViewController] isKindOfClass:[FinishPopupViewController class]]) {
        FinishPopupViewController *dst = [self destinationViewController];
        
        // add the view to the hierarchy and bring to front
        [src addChildViewController:dst];
        [src.view addSubview:dst.view];
        [src.view bringSubviewToFront:dst.view];
        
        // set the view frame
        CGRect frame;
        frame.size.height = src.view.frame.size.height;
        frame.size.width = src.view.frame.size.width;
        frame.origin.x = src.view.bounds.origin.x;
        frame.origin.y = src.view.bounds.origin.y;
        dst.view.frame = frame;
        
        [UIView animateWithDuration:0.3f animations:^{
            dst.outsideView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
            dst.insideView.backgroundColor = [UIColor whiteColor];
        }];
    } else if ([[self destinationViewController] isKindOfClass:[SOSPopupViewController class]]) {
        SOSPopupViewController *dst = [self destinationViewController];
        
        // add the view to the hierarchy and bring to front
        [src addChildViewController:dst];
        [src.view addSubview:dst.view];
        [src.view bringSubviewToFront:dst.view];
        
        // set the view frame
        CGRect frame;
        frame.size.height = src.view.frame.size.height;
        frame.size.width = src.view.frame.size.width;
        frame.origin.x = src.view.bounds.origin.x;
        frame.origin.y = src.view.bounds.origin.y;
        dst.view.frame = frame;
        
        [UIView animateWithDuration:0.3f animations:^{
            dst.outsideView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
            dst.insideView.backgroundColor = [UIColor whiteColor];
        }];
    } else if ([[self destinationViewController] isKindOfClass:[ReminderPopupViewController class]]) {
        ReminderPopupViewController *dst = [self destinationViewController];
        
        // add the view to the hierarchy and bring to front
        [src addChildViewController:dst];
        [src.view addSubview:dst.view];
        [src.view bringSubviewToFront:dst.view];
        
        // set the view frame
        CGRect frame;
        frame.size.height = src.view.frame.size.height;
        frame.size.width = src.view.frame.size.width;
        frame.origin.x = src.view.bounds.origin.x;
        frame.origin.y = src.view.bounds.origin.y;
        dst.view.frame = frame;
        
        [UIView animateWithDuration:0.3f animations:^{
            dst.outsideView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
            dst.insideView.backgroundColor = [UIColor whiteColor];
        }];
    }
}

@end
