//
//  SplashViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerFired) userInfo:nil repeats:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Splash Screen";
}

/**
 *  If settings already saved, then show the main screen, 
 *  else if user is already logged in, then show the welcome screen,
 *  otherwise show the login screen
 */
- (void)timerFired
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isLoggedIn = [defaults boolForKey:@"isLoggedIn"];
    BOOL isSettingsSaved = [defaults boolForKey:@"isSettingsSaved"];
    
    if (isSettingsSaved) {
        [self performSegueWithIdentifier:@"Main" sender:self];
    } else if (isLoggedIn) {
        [self performSegueWithIdentifier:@"Welcome" sender:self];
    } else {
        [self performSegueWithIdentifier:@"Login" sender:self];
    }
    [_timer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
