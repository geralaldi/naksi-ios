//
//  PickTwitterViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/19/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
#import "TwitterTableCell.h"
#import "TwitterAngel.h"
#import "AppDelegate.h"
#import "GAI.h"

@protocol PickTwitterDelegate <NSObject>

- (void)updateTable;

@end

/**
 *  View controller for Pick Twitter Screen
 */
@interface PickTwitterViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>

@property (weak, nonatomic) id <PickTwitterDelegate> delegate; ///< Delegate object
@property (strong, nonatomic) NSMutableArray *twitterArray; ///< Array of twitter angels
@property (strong, nonatomic) NSMutableArray *searchTwitterArray; ///< Array of twitter angels when searching
@property (strong, nonatomic) NSMutableArray *tempArray; ///< Temporary array
@property (strong, nonatomic) IBOutlet UILabel *twitterUsernameLabel; ///< Label for twitter username
@property (strong, nonatomic) IBOutlet UITableView *twitterTable; ///< Twitter followers table
@property (strong, nonatomic) IBOutlet UISearchBar *twitterSearchBar; ///< Search bar

- (IBAction)onNextClicked:(id)sender;
- (IBAction)onSaveClicked:(id)sender;

@end
