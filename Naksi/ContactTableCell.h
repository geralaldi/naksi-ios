//
//  ContactTableCell.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/6/14.
//  Copyright (c) 2014 Geraldi Kusuma Arnanto. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Subclass of UITableViewCell to be used for Contact Table
 */

@interface ContactTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *contactNumberLabel;
@property (strong, nonatomic) IBOutlet UIImageView *contactCheckBox;

@end
