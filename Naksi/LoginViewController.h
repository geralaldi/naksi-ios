//
//  LoginViewController.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/18/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentControllerViewController.h"
#import "WelcomeViewController.h"
#import "FHSTwitterEngine.h"

/**
 *  View Controller for Login Screen
 */

@interface LoginViewController : UIViewController <UIPageViewControllerDataSource>

@property (nonatomic, strong) UIPageViewController *pageViewController; ///< Page View Controller for Tour pages
@property (nonatomic, strong) NSArray *pageImages; ///< Array of Tour images
@property (strong, nonatomic) IBOutlet UIView *containerView; ///< Container View

@end
