//
//  PageContentControllerViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 4/2/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "PageContentControllerViewController.h"

@interface PageContentControllerViewController ()

@end

@implementation PageContentControllerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tourPageBackground.image = [UIImage imageNamed:self.imageFile];
    if (_pageIndex == 2)
    {
        self.loginButton.image = [UIImage imageNamed:@"twitter_login.png"];
        self.logo.image = [UIImage imageNamed:@"splash.png"];
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if (_pageIndex == 2)
    {
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginToTwitter:)];
        [singleTap setNumberOfTapsRequired:1];
        [_loginButton addGestureRecognizer:singleTap];
        [self.view addSubview:_loginButton];
        if ([[FHSTwitterEngine sharedEngine] isAuthorized])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:@"isLoggedIn"];
            [defaults synchronize];
            
            NSString *urlString = @"http://devel.wejoin.us/naksi/index.php/api/user/create";
            
            NSString *twitterUsername = [FHSTwitterEngine sharedEngine].authenticatedUsername;
            NSString *twitterId = [FHSTwitterEngine sharedEngine].authenticatedID;
            NSString *twitterToken = [FHSTwitterEngine sharedEngine].accessToken.key;
            NSString *twitterSecret = [FHSTwitterEngine sharedEngine].accessToken.secret;
            
            NSDictionary *parameters = @{
                                         @"twitter_id" : twitterId,
                                         @"twitter_name" : twitterUsername,
                                         @"twitter_token" : twitterToken,
                                         @"twitter_secret" : twitterSecret
                                        };
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            [manager POST:urlString parameters:parameters
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      NSDictionary *response = (NSDictionary *) responseObject;
                      BOOL status = (BOOL) [response objectForKey:@"status"];
                      if (status) {
                          [defaults setObject:[response objectForKey:@"user_token"] forKey:@"userToken"];
                      } else {
                          
                      }
            }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
            }];
            
            [self performSegueWithIdentifier:@"Welcome" sender:self];
        }
    }
}

- (void)loginToTwitter:(id)sender
{
    NSLog(@"Login button clicked");
    
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine] loginControllerWithCompletionHandler:^(BOOL success) {
        if (success)
        {
            NSLog(@"Login success");
        }
    }];
    [self presentViewController:loginController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
