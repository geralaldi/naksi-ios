//
//  SettingsViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/19/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _angelTable.delegate = self;
    _angelTable.dataSource = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self getProfilePicture];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                @autoreleasepool {
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [_twitterAvatar setImage:_profilePicture];
                }
            });
        }
    });
    
    _twitterName.text = [NSString stringWithFormat:@"@%@", [[FHSTwitterEngine sharedEngine] authenticatedUsername]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Settings Screen";
}

/**
 *  Get profile picture from twitter, when error occures, retry
 */
- (void)getProfilePicture
{
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
    if ([[FHSTwitterEngine sharedEngine] isAuthorized]) {
        NSString *username = FHSTwitterEngine.sharedEngine.authenticatedUsername;
        _profilePicture = [[FHSTwitterEngine sharedEngine] getProfileImageForUsername:username andSize:FHSTwitterEngineImageSizeOriginal];
        
        if ([_profilePicture isKindOfClass:[NSError class]]) {
            [self getProfilePicture];
        }
    } else {
        NSLog(@"Not authorized");
    }
}

/**
 *  Set contact and twitter angels names in angel table
 */
- (void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    _contactArray = [appDelegate getSelectedContactArray];
    _twitterArray = [appDelegate getSelectedTwitterArray];
    
    _contactNames = [[NSMutableString alloc] init];
    _twitterNames = [[NSMutableString alloc] init];
    
    for (NSInteger i=0; i < [_contactArray count];i++) {
        ContactAngel *contactAngel = [_contactArray objectAtIndex:i];
        _contactCount = _contactCount + 1;
        [_contactNames appendString:contactAngel.contactName];
        if (i != _contactArray.count - 1) {
            [_contactNames appendString:@","];
        }
    }
    
    for (NSInteger i=0; i < [_twitterArray count];i++) {
        TwitterAngel *twitterAngel = [_twitterArray objectAtIndex:i];
        _twitterCount = _twitterCount + 1;
        [_twitterNames appendString:[NSString stringWithFormat:@"@%@",twitterAngel.twitterName]];
        if (i != _twitterArray.count - 1) {
            [_twitterNames appendString:@","];
        }
    }
    [_angelTable reloadData];
}

/**
 *  Number of rows, since there are only contact and twitter angel, then there are only 2 rows
 *
 *  @param tableView TableView shown
 *  @param section   Number of section
 *
 *  @return Number of rows
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

/**
 *  Create cell for each row
 *
 *  @param tableView TableView object
 *  @param indexPath Index of row
 *
 *  @return AngelTableCell instance
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AngelTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AngelCell"];
    if (indexPath.row == 0) {
        cell.angelName.text = _contactNames;
        cell.angelCount.text = [NSString stringWithFormat:@"%i", (int) _contactArray.count];
        cell.angelCountFrame.image = [UIImage imageNamed:@"icon_contact_count"];
    } else if (indexPath.row == 1) {
        cell.angelName.text = [NSString stringWithFormat:@"%@",_twitterNames];
        cell.angelCount.text = [NSString stringWithFormat:@"%i", (int) _twitterArray.count];
        cell.angelCountFrame.image = [UIImage imageNamed:@"icon_twitter_count"];
    }
    return cell;
}

/**
 *  One of the cell is selected, if it's contact, then show Pick Contact screen,
 *  otherwise show Pick Twitter screen
 *
 *  @param tableView TableView object
 *  @param indexPath Index of row
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    if (row == 0) {
        [self performSegueWithIdentifier:@"SettingContact" sender:self];
    } else if (row == 1) {
        [self performSegueWithIdentifier:@"SettingTwitter" sender:self];
    }
}

/**
 *  Reload table content
 */
- (void)updateTable
{
    [_angelTable reloadData];
}

/**
 *  Save button from summary child view was clicked, save the angels to file using AppDelegate's method
 */
- (void)saveClicked
{
    [_angelTable setUserInteractionEnabled:NO];
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [appDelegate saveDataToDisk];
    
    NSMutableArray *selectedContactArray = [appDelegate getSelectedContactArray];
    NSMutableArray *selectedTwitterArray = [appDelegate getSelectedTwitterArray];
    
    NSDictionary *contactDictionary = [[NSDictionary alloc] init];
    NSDictionary *twitterDictionary = [[NSDictionary alloc] init];
    
    NSMutableArray *contacts = [[NSMutableArray alloc] init];
    NSMutableArray *twitters = [[NSMutableArray alloc] init];
    
    for (int i=0; i < [selectedContactArray count]; i++) {
        NSDictionary *contact = [[NSDictionary alloc] init];
        ContactAngel *contactAngel = [selectedContactArray objectAtIndex:i];
        
        contact = [NSDictionary dictionaryWithObjectsAndKeys:contactAngel.contactName, @"info", contactAngel.contactNumber, @"contact", nil];
        [contacts addObject:contact];
    }
    
    for (int i=0; i < [selectedTwitterArray count]; i++) {
        NSDictionary *twitter = [[NSDictionary alloc] init];
        TwitterAngel *twitterAngel = [selectedTwitterArray objectAtIndex:i];
        
        twitter = [NSDictionary dictionaryWithObjectsAndKeys:twitterAngel.twitterName, @"info", twitterAngel.twitterId, @"contact", nil];
        [twitters addObject:twitter];
    }
    
    contactDictionary = ([selectedContactArray count] > 0) ? [NSDictionary dictionaryWithObjectsAndKeys:contacts, @"id", @"contact", @"type", nil] : [NSDictionary dictionaryWithObjectsAndKeys:@"null", @"id", @"contact", @"type", nil];
    twitterDictionary = ([selectedTwitterArray count] > 0) ? [NSDictionary dictionaryWithObjectsAndKeys:twitters, @"id", @"twitter", @"type", nil] : [NSDictionary dictionaryWithObjectsAndKeys:@"null", @"id", @"twitter", @"type", nil];
    
    [self sendToServer:contactDictionary];
    [self sendToServer:twitterDictionary];
}

/**
 *  Send angels to server
 *
 *  @param dictionary JSONObject of the request to be sent
 */
- (void)sendToServer:(NSDictionary *)dictionary
{
    NSString *userToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"userToken"];
    NSString *urlString = [NSString stringWithFormat:@"http://devel.wejoin.us/naksi/index.php/api/angel/update?api_key=%@", userToken];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:urlString parameters:dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSDictionary *response = (NSDictionary *) responseObject;
              BOOL status = (BOOL) [response objectForKey:@"status"];
              if (status) {
                  NSLog(@"Angel saved");
                  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSettingsSaved"];
              } else {
                  NSLog(@"%@", response);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
          }];
    
}

/**
 *  User is riding, set angel table to be unclickable
 */
- (void)isRiding
{
    [_angelTable setUserInteractionEnabled:NO];
}

/**
 *  Show finish popup dialog
 */
- (void)showFinishDialogOnSettings
{
    [self performSegueWithIdentifier:@"FinishPopup" sender:nil];
}

/**
 *  Show SOS popup dialog
 */
- (void)showSOSDialogOnSettings
{
    [self performSegueWithIdentifier:@"SOSPopup" sender:nil];
}

/**
 *  Enable editing for angel table
 */
- (void)enableEdit
{
    [_angelTable setUserInteractionEnabled:YES];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isSettingsSaved"];
}

/**
 *  Send delegate to execute updateTravelDuration
 */
- (void)updateTravelOnSettings
{
    [_delegate updateTravelDuration];
}

/**
 *  Prepare for segue, set Container View's delegate to this
 *
 *  @param segue  Segue
 *  @param sender Sender
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Child"]) {
        ContainerViewController *container = (ContainerViewController *) segue.destinationViewController;
        container.delegate = self;
        if ([_segueIdentifier isEqualToString:@"Riding"]) {
            container.currentSegueIdentifier = @"Riding";
            container.expectedTime = _expectedTime;
        } else if ([_segueIdentifier isEqualToString:@"Setting"]) {
            container.currentSegueIdentifier = @"Summary";
        }
    } 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
