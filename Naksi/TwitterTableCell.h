//
//  TwitterTableCell.h
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/20/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Subclass of UITableViewCell for twitter followers 
 */
@interface TwitterTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *twitterCheckBox;
@property (strong, nonatomic) IBOutlet UILabel *twitterNameLabel;

@end
