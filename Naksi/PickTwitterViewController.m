//
//  PickTwitterViewController.m
//  Naksi
//
//  Created by Geraldi Kusuma Arnanto on 3/19/14.
//  Copyright (c) 2014 GIT. All rights reserved.
//

#import "PickTwitterViewController.h"

@interface PickTwitterViewController ()

@end

@implementation PickTwitterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    _tempArray = [appDelegate getTwitterArray];
    
    _twitterArray = [[NSMutableArray alloc] init];
    _twitterArray = _tempArray;
    _twitterTable.delegate = self;
    _twitterTable.dataSource = self;
    _twitterTable.allowsMultipleSelection = YES;
    
    self.searchDisplayController.searchResultsTableView.delegate = self;
    self.searchDisplayController.searchResultsTableView.dataSource = self;
    self.searchDisplayController.searchResultsTableView.allowsMultipleSelection = YES;
    
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
    _twitterUsernameLabel.text = [NSString stringWithFormat:@"@%@",[[FHSTwitterEngine sharedEngine] authenticatedUsername]];
    
    /**
     *  Get user's followers in the background, then update the table
     */
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self getTwitterFollowers];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                @autoreleasepool {
                    [_twitterTable reloadData];
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                }
            });
        }
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Pick Twitter Screen";
}

/**
 *  Get user's followers on twitter. When there is any error, retry
 *  otherwise split the followers' IDs
 */
- (void)getTwitterFollowers
{
    if ([[FHSTwitterEngine sharedEngine] isAuthorized]) {
        NSMutableDictionary *response = [[FHSTwitterEngine sharedEngine] getFollowersIDs];
        if ([response isKindOfClass:[NSError class]]) {
            [self getTwitterFollowers];
        } else {
            [self splitTwitterFollowers:response];
        }
    }
}

/**
 *  We have to split the followers' IDs into maximum 100 per parts, it's twitter's fault
 *  after we split the IDs and save them into an array, we get each 100 IDs' properties
 *  in twitter to get their names by using lookupUser API endpoint, then save all of them
 *  into twitterArray
 *
 *  @param response JSON response from getFollowersIDs
 */
- (void)splitTwitterFollowers:(NSMutableDictionary *)response
{
    BOOL existError = YES;
    NSMutableArray *twitterIDs = [[NSMutableArray alloc] init];
    for (NSDictionary *item in [response objectForKey:@"ids"])  {
        [twitterIDs addObject:item];
    }
    
    _tempArray = [[NSMutableArray alloc] init];
    for (NSInteger i=0 ; i < [twitterIDs count] ; i+=100)  {
        NSInteger length = ([twitterIDs count] - i) >= 100 ? 100 : ([twitterIDs count] - i);
        NSArray *split = [[NSArray alloc] init];
        NSRange range = NSMakeRange(i, length);
        split = [twitterIDs subarrayWithRange:range];
        
        NSMutableArray *twitterFollowers = [[FHSTwitterEngine sharedEngine] lookupUsers:split areIDs:YES];
        if ([twitterFollowers isKindOfClass:[NSError class]]) {
            existError = YES;
            break;
        } else {
            existError = NO;
            for (NSDictionary *item in twitterFollowers) {
                TwitterAngel *twitterAngel = [[TwitterAngel alloc] init];
                twitterAngel.twitterName = [item objectForKey:@"screen_name"];
                twitterAngel.twitterId = [item objectForKey:@"id"];
                [_tempArray addObject:twitterAngel];
            }
        }
    }
    
    if (!existError) {
        if ([_tempArray count] == [_twitterArray count]) {
            return;
        } else {
            _twitterArray = _tempArray;
        }
        
        NSArray *sortedArray = [_twitterArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSString *first = ((TwitterAngel *)obj1).twitterName;
            NSString *second = ((TwitterAngel *)obj2).twitterName;
            return [first compare:second];
        }];
        
        _twitterArray = [[NSMutableArray alloc] initWithArray:sortedArray];
        for (int i=0 ; i < [_twitterArray count];i++) {
            [[_twitterArray objectAtIndex:i] setListPosition:i];
        }
    } else {
        [self splitTwitterFollowers:response];
    }
}

/**
 *  Number of rows in one table section, if tableView is not regular table,
 *  then its number is the length of Search Twitter Array, otherwise it is
 *  Twitter Array's
 *
 *  @param tableView TableView object
 *  @param section   Section number
 *
 *  @return Number of rows
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView != _twitterTable) {
        return [_searchTwitterArray count];
    }
    return [_twitterArray count];
}

/**
 *  Create cell for each indexpath/row
 *
 *  @param tableView TableView used
 *  @param indexPath Index of row
 *
 *  @return TwitterTableCell instance
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwitterTableCell *cell = [_twitterTable dequeueReusableCellWithIdentifier:@"TwitterCell"];
    
    if (cell == nil) {
        cell = [[TwitterTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TwitterCell"];
    }
    
    TwitterAngel *angel = nil;
    
    if (tableView != _twitterTable) {
        angel = [_searchTwitterArray objectAtIndex:indexPath.row];
    } else {
        angel = [_twitterArray objectAtIndex:indexPath.row];
    }
    cell.twitterNameLabel.text = [NSString stringWithFormat:@"@%@",angel.twitterName];
    if (angel.isClicked) {
        [tableView selectRowAtIndexPath:indexPath
                               animated:NO
                         scrollPosition:UITableViewScrollPositionNone];
    }
    return cell;
}

/**
 *  When selecting twitter follower, change the checkbox image, and set the twitter's clicked property to YES
 *
 *  @param tableView TableView object
 *  @param indexPath Index of row selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != _twitterTable) {
        NSInteger row = indexPath.row;
        TwitterAngel *twitterAngel = [_searchTwitterArray objectAtIndex:row];
        BOOL selected = twitterAngel.clicked;
        [[_searchTwitterArray objectAtIndex:row] setClicked:!selected];
        [[_twitterArray objectAtIndex:twitterAngel.listPosition] setClicked:!selected];
    } else {
        NSInteger row = indexPath.row;
        TwitterAngel *twitterAngel = [_twitterArray objectAtIndex:row];
        BOOL selected = twitterAngel.clicked;
        [[_twitterArray objectAtIndex:row] setClicked:!selected];
    }
    [_twitterTable reloadData];
}

/**
 *  When deselect a follower, change the checkbox image, and set the twitter's clicked property to NO
 *
 *  @param tableView TableView object
 *  @param indexPath Index of row selected
 */
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != _twitterTable) {
        NSInteger row = indexPath.row;
        TwitterAngel *twitterAngel = [_searchTwitterArray objectAtIndex:row];
        BOOL selected = twitterAngel.clicked;
        [[_twitterArray objectAtIndex:twitterAngel.listPosition] setClicked:!selected];
    } else {
        NSInteger row = indexPath.row;
        TwitterAngel *twitterAngel = [_twitterArray objectAtIndex:row];
        BOOL selected = twitterAngel.clicked;
        [[_twitterArray objectAtIndex:row] setClicked:!selected];
    }
    [_twitterTable reloadData];
}

/**
 *  When searching, table shown should be reloaded based on the search string
 *
 *  @param controller   UISearchDisplayController object
 *  @param searchString Input string
 *
 *  @return Whether the table should be reloaded
 */
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

/**
 *  Search text based on input in twitterArray's name, with format twitterName contains the search string
 *
 *  @param searchText The search string
 *  @param scope      Scope
 */
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"twitterName contains[c] %@", searchText];
    _searchTwitterArray = [NSMutableArray arrayWithArray:[_twitterArray filteredArrayUsingPredicate:resultPredicate]];
}

/**
 *  When user begins to search in iOS 7 devices, we have to move the search bar up
 *  so the views behind the search bar are visible
 *
 *  @param controller SearchDisplayController
 */
- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    controller.searchResultsDataSource = self;
    controller.searchResultsDelegate = self;
    controller.searchResultsTableView.delegate = self;
    controller.searchResultsTableView.allowsMultipleSelection = YES;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        [UIView animateWithDuration:0.25 animations:^{
            _twitterSearchBar.transform = CGAffineTransformMakeTranslation(0, -120);
        }];
    }
}

/**
 *  When the search is ending, return the search bar position to its default
 *
 *  @param controller SearchDisplayController
 */
- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        [UIView animateWithDuration:0.25 animations:^{
            _twitterSearchBar.transform = CGAffineTransformIdentity;
        }];
    }
}

/**
 *  Next button clicked, save the selected followers in App Delegate, then show Settings Screen
 *
 *  @param sender Sender
 */
- (IBAction)onNextClicked:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i < _twitterArray.count ; i++) {
        TwitterAngel *angel = [_twitterArray objectAtIndex:i];
        if (angel.clicked) {
            [tempArray addObject:angel];
        }
    }
    [appDelegate setTwitterArray:_twitterArray];
    [appDelegate setSelectedTwitterArray:tempArray];
    
    [self performSegueWithIdentifier:@"Settings" sender:self];
}

/**
 *  Save button clicked, this is when changing the twitter angels, save the selected to App Delegate
 *  when done, dismiss the screen and update the angel table in Settings screen
 *
 *  @param sender Sender
 */
- (IBAction)onSaveClicked:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i < _twitterArray.count ; i++) {
        TwitterAngel *angel = [_twitterArray objectAtIndex:i];
        if (angel.clicked) {
            [tempArray addObject:angel];
        }
    }
    [appDelegate setTwitterArray:_twitterArray];
    [appDelegate setSelectedTwitterArray:tempArray];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
